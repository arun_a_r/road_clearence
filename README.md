# Using multi-agent robots to clear a given arena
## Run

```
cd waste_removal_k_1_and_2/
argos3 -c settings.argos
```

## Aim

**To remove obstacles in the arena.**  
Two types of robots **helpers** and **spotters** to remove two different types  
of obstacles both cylinder shaped, small obstacles with a radius of 0.1 and  
big obstacles with a radius of 0.2, both with a height of 0.1.

## Method
As mentioned above we have to remove two types of obstacles of different sizes.  
In this experiment the smaller obstacles are moved by single robot while the  
larger obstacles need co-ordination of two robots.  

The spotter robot that searches for obstacles and moves it to the nearest border  
in case it is a small obstacle but if it is a large obstacle it calls one of the  
helper robots to help it in moving to the nearest border.

## Spotter
The robot that searches for the obstacles to be removed

### Sensors
1. positioning
2. colored_blob_omnidirectional_camera
3. footbot_motor_ground
4. differential_steering
5. proximity
6. range_and_bearing

### Actuators
1. differential_steering
2. turret
3. gripper
4. leds
5. range_and_bearing

## helpers
The robots that help the spotters in moving the spotted large obstacles to the designated arena

### Sensors
1. positioning
2. colored_blob_omnidirectional_camera
3. footbot_motor_ground
4. differential_steering
5. proximity
6. range_and_bearing

### Actuators
1. differential_steering
2. turret
3. gripper
4. leds
5. range_and_bearing
