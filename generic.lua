--------------------------------------------------------------------------------
--------------------------------generic-----------------------------------------
--------------------------------------------------------------------------------
l = 0
r = 0
direction = 0
count_time = 0
count_time = 0
go_ahead = 0

msg = {}
msg[1] = "Help"
msg[2] = "Willing"
msg[3] = "ACK_initiator"
msg[4] = "ACK_non-initator"
msg[5] = "beacon"
msg[6] = "grabbed road-blocker"
msg[7] = "go north"
msg[8] = "go west"
msg[9] = "go south"
msg[10] = "go east"
msg[11] = "home orientation set"
msg[12] = "let's go"
--------------------------------------------------------------------------------
-----------------------------function init--------------------------------------
--------------------------------------------------------------------------------
function init()
    self_addr = addr(robot.id)
    log(robot.id," = ",id)
    state = "search"
    prev_state = "dummy"
    int_state = "listening"
    prev_int_state = "dummy"
    robot.colored_blob_omnidirectional_camera.enable()
    robot.turret.set_position_control_mode()
end
--------------------------------------------------------------------------------
------------------------------Function Step-------------------------------------
--------------------------------------------------------------------------------
function step()
    if state ~= prev_state then
        log(self_addr,"=",state)
    end
    prev_state = state
    if state == "search" then
        robot.leds.set_all_colors(0,0,0)
        search()
    elseif state == "choose" then
        robot.leds.set_all_colors("green")
        choose()
    elseif state == "approach" then
        robot.leds.set_all_colors("green")
        approach()
    elseif state == "grab" then
        robot.leds.set_all_colors("green")
        grab()
    elseif state == "call" then
        robot.leds.set_all_colors("green")
        call()
    elseif state == "specific_call" then
        robot.leds.set_all_colors("green")
        specific_call()
    elseif state == "waiting_for_final_ack" then
        robot.leds.set_all_colors("green")
        waiting_for_final_ack()
    elseif state == "waiting" then
        robot.leds.set_all_colors("green")
        waiting()
    elseif state == "determine_orient" then
        robot.leds.set_all_colors("green")
        determine_orient()
    elseif state == "go_home" then
        robot.leds.set_all_colors("green")
        go_home()
    elseif state == "home" then
        robot.leds.set_all_colors("green")
        home()
    elseif state == "nearest_border_orient" then
        robot.leds.set_all_colors("green")
        nearest_border_orient()
    end
end
--------------------------------------------------------------------------------
-------------------------------reset & destroy----------------------------------
--------------------------------------------------------------------------------
function reset()
end
function destroy()
end
--------------------------------------------------------------------------------
----------------------------------addr fn---------------------------------------
-----------A hash function that decides the address of the robot----------------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
--------------------------------Function search---------------------------------
-------------We roam around the arena searching for waste drums-----------------
--------------------------------------------------------------------------------
function search()
    if int_state ~= prev_int_state then
        log(self_addr,"=",int_state)
    end
    prev_int_state = int_state
    --robot.leds.set_all_colors("green")
    if int_state == "listening" then
        sensingLeft =     robot.proximity[3].value +
                          robot.proximity[4].value +
                          robot.proximity[5].value +
                          robot.proximity[6].value +
                          robot.proximity[2].value +
                          robot.proximity[1].value

        sensingRight =    robot.proximity[19].value +
                          robot.proximity[20].value +
                          robot.proximity[21].value +
                          robot.proximity[22].value +
                          robot.proximity[24].value +
                          robot.proximity[23].value
    --------To make sure of returning to road-----------------------------
        search_x = robot.positioning.position.x
        search_y = robot.positioning.position.y
        search_sign = robot.positioning.orientation.axis.z
        search_angle = robot.positioning.orientation.angle
    --------This ensures that we are not trying to lift already moved boxes---------
        if #robot.colored_blob_omnidirectional_camera >= 1 then
            check = 0
            for i = 1, #robot.colored_blob_omnidirectional_camera do
                if robot.colored_blob_omnidirectional_camera[i].color.green > check then
                    check = robot.colored_blob_omnidirectional_camera[i].color.green
                end
            end
            if check == 0 then
                state = "choose"
            end
        end
    -----------------This is obstacle avoidance navigation--------------------------
        if sensingLeft ~= 0 then
            robot.wheels.set_velocity(7,3)
        elseif sensingRight ~= 0 then
            robot.wheels.set_velocity(3,7)
        elseif robot.motor_ground[1].value > 0.5 and robot.motor_ground[4].value < 0.5 then
            robot.wheels.set_velocity(7,3)
        elseif robot.motor_ground[4].value > 0.5 and robot.motor_ground[1].value < 0.5 then
            robot.wheels.set_velocity(3,7)
        elseif search_y > 1.25 then --go right
            if search_x < 0 then
                if (search_sign == 1 and search_angle > 5 and search_angle < 5.5) or (search_sign == -1 and search_angle > 0.8 and search_angle < 1.2) then
                    robot.wheels.set_velocity(10,10)
                else
                    robot.wheels.set_velocity(10,3)
                end
            elseif search_x >= 0 then
                if (search_sign == 1 and search_angle > 3.5 and search_angle < 4) or (search_sign == -1 and search_angle > 2 and search_angle < 2.5) then
                    robot.wheels.set_velocity(10,10)
                else
                    robot.wheels.set_velocity(10,3)
                end
            end
        elseif search_y < -1.25 then --go left
            if search_x < 0 then
                if (search_sign == 1 and search_angle > 0.8 and search_angle < 1.2) or (search_sign == -1 and search_angle > 5 and search_angle < 5.5) then
                    robot.wheels.set_velocity(10,10)
                else
                    robot.wheels.set_velocity(3,10)
                end
            elseif search_x >= 0 then
                if (search_sign == -1 and search_angle > 3.5 and search_angle < 4) or (search_sign == 1 and search_angle > 2 and search_angle < 2.5) then
                    robot.wheels.set_velocity(10,10)
                else
                    robot.wheels.set_velocity(3,10)
                end
            end
        else
            robot.wheels.set_velocity(10,10)
        end

        --listening part
        calls = {}
        if #robot.range_and_bearing > 0 then
            for i = 1,#robot.range_and_bearing do
                if robot.range_and_bearing[i].data[2] == 255 then
                    table.insert(calls, robot.range_and_bearing[i])
                end
            end
        end
        if #calls > 0 then
            distance = 10000
            caller = 0
            for i = 1, #calls do
                if calls[i].range < distance then
                    distance = calls[i].range
                    caller = calls[i]
                end
            end
        end
        if #calls > 0 and caller ~= 0 then
            log(self_addr,"received ", msg[caller.data[3]]," from ", caller.data[1])
            int_state = "responding_to_braodcast"
        end

    elseif int_state == "responding_to_braodcast" then
        responding_to_braodcast()
    elseif int_state == "waiting_for_final_call" then
        waiting_for_final_call()
    elseif int_state == "give_final_ack" then
        give_final_ack()
    elseif int_state == "orient" then
        orient()
    elseif int_state == "approach_caller" then
        approach_caller()
    elseif int_state == "grab_robot" then
        grab_robot()
    elseif int_state == "final_orientation" then
        final_orientation()
    elseif int_state == "home" then
        home()
    end
end
--------------------------------------------------------------------------------
------------------------------function choose-----------------------------------
------------------------Choose the nearest obstacle-----------------------------
--------------------------------------------------------------------------------
function choose()
    if #robot.colored_blob_omnidirectional_camera == 0 then
        state = "search"
    else
        robot.wheels.set_velocity(0,0)
        closest = robot.colored_blob_omnidirectional_camera[1]
        dist = robot.colored_blob_omnidirectional_camera[1].distance
        ang =  robot.colored_blob_omnidirectional_camera[1].angle
        for i = 1, #robot.colored_blob_omnidirectional_camera do

            if (dist > robot.colored_blob_omnidirectional_camera[i].distance and
                robot.colored_blob_omnidirectional_camera[i].color.green == 0) and
                (robot.colored_blob_omnidirectional_camera[i].color.blue == 255 or
                robot.colored_blob_omnidirectional_camera[i].color.red == 255) then
                closest = robot.colored_blob_omnidirectional_camera[i]
                dist = closest.distance
                ang = closest.angle

            end

        end
        if ang > 0.1 then
            robot.wheels.set_velocity(-1,1)
        elseif ang < -0.1 then
            robot.wheels.set_velocity(1,-1)
        elseif ang >= -0.1 and ang <= 0.1 then
            state = "approach"
        end
    end
end
--------------------------------------------------------------------------------
---------------------------------function approach------------------------------
---------------------------------Reach the obstacle-----------------------------
--------------------------------------------------------------------------------
function approach()
    if #robot.colored_blob_omnidirectional_camera > 0 then
        x = 0
        for i = 1, 24 do --some modification must be done here as we need not check
                         --all proximity sensors then ones located in front shall do
            if x < robot.proximity[i].value then
                x = robot.proximity[i].value
            end
        end
    -------trying to keep the orientation while approaching the obstacle------------
        dist = robot.colored_blob_omnidirectional_camera[1].distance
        ang =  robot.colored_blob_omnidirectional_camera[1].angle

        for i = 1, #robot.colored_blob_omnidirectional_camera do

            if dist > robot.colored_blob_omnidirectional_camera[i].distance and
                (robot.colored_blob_omnidirectional_camera[i].color.red == 255 or
                robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then

                dist = robot.colored_blob_omnidirectional_camera[i].distance
                ang = robot.colored_blob_omnidirectional_camera[i].angle

            end
        end
        if ang > 0 then
            robot.wheels.set_velocity(5,6)
        end
        if ang < 0 then
            robot.wheels.set_velocity(6,5)
        end
        if ang == 0 then
            robot.wheels.set_velocity(5,5)
        end
    -------------trying to slow down when reaching near the obstacle----------------
        if x >= 0.5 then
            robot.wheels.set_velocity((1 - x) * 10, (1 - x) * 10)
        end
        if x >= 0.9 then
            robot.wheels.set_velocity(0,0)
            state = "grab"
        end
    else
        state = "search"
    end
end
--------------------------------------------------------------------------------
---------------------------------function grab----------------------------------
---------------------------Grab the choosen obstacle----------------------------
--------------------------------------------------------------------------------
function grab()
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value == 1 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if count_time == 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        count_time = 0
        if closest.color.blue == 255 then
            state = "call"
        elseif closest.color.red == 255 then
            state = "nearest_border_orient"
        end
    end
end
--------------------------------------------------------------------------------
---------------------------------function call----------------------------------
-------------------------CAll nearest robot for help----------------------------
--------------------------------------------------------------------------------
function call()
    broadcast_msg = compose(255,1)
    robot.range_and_bearing.set_data(broadcast_msg)
    log(self_addr," sending ",msg[1], "to all")
    distance = 10000
    nearest = 0
    if #robot.range_and_bearing > 0 then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 2 and
                robot.range_and_bearing[i].range < distance then

                distance = robot.range_and_bearing[i].range
                nearest = robot.range_and_bearing[i]
            end
        end
    end
    if nearest ~= 0 then
        state = "specific_call"
    end
end
-------------------------------------------------------------------------------
-------------------------Function specific_call--------------------------------
----------This function calls a specific robot to come-------------------------
-------------------------------------------------------------------------------
function specific_call()
    specific_msg = compose(nearest.data[1],3)
    robot.range_and_bearing.set_data(specific_msg)
    log(self_addr," sending ",msg[3]," to ",nearest.data[1])
    state = "waiting_for_final_ack"
end
--------------------------------------------------------------------------------
---------------------Function waiting_for_final_ack-----------------------------
--------------------------------------------------------------------------------
function waiting_for_final_ack()
    if #robot.range_and_bearing > 0 then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[3] == 4 then
                log(self_addr,"received final ack from",robot.range_and_bearing[i].data[1])
                state = "waiting"
            end
        end
    end
end
--------------------------------------------------------------------------------
-----------------------------function compose-----------------------------------
---This function composes message takes two arguments to address and message----
--------------------------------------------------------------------------------
function compose(to_addr,message)
     comp_msg = {self_addr,to_addr,message}
     return comp_msg
 end
--------------------------------------------------------------------------------
------------------------------Function waiting----------------------------------
--------------------------------------------------------------------------------
function waiting()
    beacon = compose(nearest.data[1],5)
    robot.range_and_bearing.set_data(beacon)
    if #robot.range_and_bearing > 0 then

        for i = 1,#robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 6 then
                state = "determine_orient"
                log (self_addr," received ",msg[robot.range_and_bearing[i].data[3]]," from ",robot.range_and_bearing[i].data[1])
            end

        end
    end
end
--------------------------------------------------------------------------------
-------------------------Function determine_orient------------------------------
--------------------------------------------------------------------------------
function determine_orient()
    --x = robot.positioning.position.x
    y = robot.positioning.position.y
    --[[
    if (x > 0 and y > 0 and x >= y) or
        (x > 0 and y < 0 and math.abs(x) > math.abs(y)) then --up/north

        orient_msg = compose(nearest.data[1],7)
        robot.range_and_bearing.set_data(orient_msg)
        state = "go_home"

    elseif (x > 0 and y > 0 and y > x) or
            (x < 0 and y > 0 and math.abs(y) > math.abs(x)) then--left/west

            orient_msg = compose(nearest.data[1],8)
            robot.range_and_bearing.set_data(orient_msg)
            state = "go_home"

    elseif (x < 0 and y > 0 and math.abs(x) > math.abs(y)) or
            (x < 0 and y < 0 and math.abs(x) > math.abs(y)) then --bottom/south

            orient_msg = compose(nearest.data[1],9)
            robot.range_and_bearing.set_data(orient_msg)
            state = "go_home"

    elseif (x > 0 and y < 0 and math.abs(x) < math.abs(y)) or
            (x < 0 and y < 0 and math.abs(x) < math.abs(y)) then --right/east

            orient_msg = compose(nearest.data[1],10)
            robot.range_and_bearing.set_data(orient_msg)
            state = "go_home"
    end
    ]]--
    if y <= 0 then --go right
        orient_msg = compose(nearest.data[1],10)
        robot.range_and_bearing.set_data(orient_msg)
        state = "go_home"
    else
        orient_msg = compose(nearest.data[1],8)
        robot.range_and_bearing.set_data(orient_msg)
        state = "go_home"
    end
end
--------------------------------------------------------------------------------
-----------------------------function go home-----------------------------------
--------------------------------------------------------------------------------
function go_home()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if orient_msg[3] == 7 then
        if (sign == 1 and angle < 0.1) or (sing == -1 and angle > 6.1) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 8 then
        if (sign == 1 and angle > 1.4 and angle < 1.6) or (sign == -1 and angle > 4.6 and angle < 4.7) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 9 then
        if angle > 3 and angle < 3.2 then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 10 then
        if (sign == 1 and angle > 4.6 and angle < 4.7) or (sign == -1 and angle > 1.4 and angle < 1.6) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(1,-1)
        end
    end
    if #robot.range_and_bearing then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 11 then

                lets_go_msg = compose(nearest.data[1],12)
                robot.range_and_bearing.set_data(lets_go_msg)
                go_ahead = 0
                state = "home"

            end
        end
    end
end
--------------------------------------------------------------------------------
-------------------------------function home------------------------------------
--------------------------------------------------------------------------------
function home()
    robot.wheels.set_velocity(10,10)
    ground = robot.motor_ground[1].value +
             robot.motor_ground[2].value +
             robot.motor_ground[3].value +
             robot.motor_ground[4].value
    if ground == 4 then
        count_time = count_time + 1
        if count_time == 100 then
            count_time = 0
            robot.gripper.unlock()
            robot.turret.set_position_control_mode()
            state = "search"
            int_state ="listening"
        end
    end
end
--------------------------------------------------------------------------------
-------------------------function nearest_border_orient-------------------------
----------Determines where the nearest border is located up/down/left right-----
--------------------------------------------------------------------------------
function nearest_border_orient()
    y = robot.positioning.position.y
    angle = robot.positioning.orientation.angle
    sign = robot.positioning.orientation.axis.z
    --[[if (x >= 0 and y >= 0) and x >=y then --first quadrant --go top
        if (sign == 1 and angle < 0.1) or (sing == -1 and angle > 6.1) then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    elseif (x > 0 and y > 0) and y > x then --first quadrant --go left
        if (sign == 1 and angle > 1.4 and angle < 1.6) or (sign == -1 and angle > 4.6 and angle < 4.7) then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    elseif (x < 0 and y > 0) and -x > y then --second quadrant --go bottom
        if angle > 3 and angle < 3.2 then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    elseif (x < 0 and y > 0) and y > -x then --second quadrant --go left
        if (sign == 1 and angle > 1.4 and angle < 1.6) or (sign == -1 and angle > 4.6 and angle < 4.7) then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    elseif (x > 0 and y < 0) and math.abs(x) > math.abs(y) then --fourth quadrant --go top
        if (sign == 1 and angle < 0.1) or (sing == -1 and angle > 6.1) then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    elseif (x > 0 and y < 0) and math.abs(x) < math.abs(y) then --fourth quadrant --go right
        if (sign == 1 and angle > 4.6 and angle < 4.7) or (sign == -1 and angle > 1.4 and angle < 1.6) then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    elseif (x < 0 and y < 0) and math.abs(x) > math.abs(y) then --third quadrant --go bottom
        if angle > 3 and angle < 3.2 then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    elseif (x < 0 and y < 0) and math.abs(x) < math.abs(y) then --third quadrant --go right
        if (sign == 1 and angle > 4.6 and angle < 4.7) or (sign == -1 and angle > 1.4 and angle < 1.6) then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    end
    ]]--
    if y <= 0 then
        if (sign == 1 and angle > 4.6 and angle < 4.7) or (sign == -1 and angle > 1.4 and angle < 1.6) then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    elseif y > 0 then
        if (sign == 1 and angle > 1.4 and angle < 1.6) or (sign == -1 and angle > 4.6 and angle < 4.7) then
            state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    end

end
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-----------------------Function responding_to_braodcast-------------------------
--------------------------------------------------------------------------------
function responding_to_braodcast()
    --robot.leds.set_all_colors("red")
    send_ack = compose(caller.data[1],2)
    robot.range_and_bearing.set_data(send_ack)
    log(self_addr," sending ",msg[2]," to ",caller.data[1])
    int_state = "waiting_for_final_call"
end
--------------------------------------------------------------------------------
-----------------------Function waiting_for_final_call--------------------------
--------------------------------------------------------------------------------
function waiting_for_final_call()
    if #robot.range_and_bearing > 0 then
        for i =1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr
                and robot.range_and_bearing[i].data[3] == 3 then

                caller = robot.range_and_bearing[i]
                log (self_addr,"received",msg[3]," from ",robot.range_and_bearing[i].data[1])
                int_state = "give_final_ack"
            elseif robot.range_and_bearing[i].data[3] == 3
                and robot.range_and_bearing[i].data[2] ~= self_addr then

                int_state = "listening"
            end
        end
    end
end
--------------------------------------------------------------------------------
-------------------------function give_final_ack--------------------------------
--------------------------------------------------------------------------------
function give_final_ack()
    final_ack = compose(caller.data[1],4)
    robot.range_and_bearing.set_data(final_ack)
    log(self_addr," sending ",msg[4]," to ",caller.data[1])
    int_state = "orient"
end
--------------------------------------------------------------------------------
-----------------------------function compose-----------------------------------
---This function composes message takes two arguments to address and message----
--------------------------------------------------------------------------------
function compose(to_addr,message)
     comp_msg = {self_addr,to_addr,message}
     return comp_msg
 end
--------------------------------------------------------------------------------
------------------------------function orient-----------------------------------
--------------------------------------------------------------------------------
function orient()

    if l ~= 0 and l ~= robot.wheels.left_velocity then
        int_state = "approach_caller"
    end
    if #robot.range_and_bearing > 0 then

        ang = 200
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr
                and robot.range_and_bearing[i].data[3] == 5 then

                ang = robot.range_and_bearing[i]
            end
        end
        if ang ~= 200 then
            if (ang.horizontal_bearing < 0.25 and
                ang.horizontal_bearing > -0.25) then

                int_state = "approach_caller"
            elseif ang.horizontal_bearing > 0 then
                robot.wheels.set_velocity(-1,1)
            elseif ang.horizontal_bearing < 0 then
                robot.wheels.set_velocity(1,-1)
            end
        end
    end
    l = robot.wheels.left_velocity
    r = robot.wheels.right_velocity
end
--------------------------------------------------------------------------------
-------------------------function approach_caller-------------------------------
--------------------------------------------------------------------------------
function approach_caller()
    sensingLeft =     robot.proximity[3].value +
                      robot.proximity[4].value +
                      robot.proximity[5].value +
                      robot.proximity[6].value +
                      robot.proximity[2].value +
                      robot.proximity[1].value

    sensingRight =    robot.proximity[19].value +
                      robot.proximity[20].value +
                      robot.proximity[21].value +
                      robot.proximity[22].value +
                      robot.proximity[24].value +
                      robot.proximity[23].value

    if #robot.range_and_bearing then

        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 5 then

                if robot.range_and_bearing[i].range > 75 then

                    if sensingLeft ~= 0 then
                        robot.wheels.set_velocity(7,3)
                    elseif sensingRight ~= 0 then
                        robot.wheels.set_velocity(3,7)
                    elseif robot.range_and_bearing[i].horizontal_bearing > 0 and
                            robot.range_and_bearing[i].horizontal_bearing <= 0.5 then
                        robot.wheels.set_velocity(9,10)
                    elseif robot.range_and_bearing[i].horizontal_bearing >0.5 then
                        robot.wheels.set_velocity(3,7)
                    elseif robot.range_and_bearing[i].horizontal_bearing < 0 and
                            robot.range_and_bearing[i].horizontal_bearing >= -0.5 then
                        robot.wheels.set_velocity(10,9)
                    elseif robot.range_and_bearing[i].horizontal_bearing < -0.5 then
                        robot.wheels.set_velocity(7,3)
                    end
                elseif robot.range_and_bearing[i].range < 50 then

                    if #robot.colored_blob_omnidirectional_camera > 0 then
                        dist = 100
                        this_ang =  robot.colored_blob_omnidirectional_camera[1].angle
                        for i = 1, #robot.colored_blob_omnidirectional_camera do
                            --log(i)
                            if robot.colored_blob_omnidirectional_camera[i].color.blue == 255 and
                                robot.colored_blob_omnidirectional_camera[i].distance < dist then

                                dist = robot.colored_blob_omnidirectional_camera[i].distance
                                this_ang = robot.colored_blob_omnidirectional_camera[i].angle

                            end
                        end
                        --log(dist)
                        if dist > 30 and this_ang > 0 then
                            robot.wheels.set_velocity(3,7)
                        elseif dist > 30 and this_ang < 0 then
                            robot.wheels.set_velocity(7,3)
                        elseif dist < 30 and dist > 29 then
                            robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                        elseif dist < 29 and dist > .1 then
                            int_state = "grab_robot"
                        end
                    end
                end
            end
        end
    end
end
--------------------------------------------------------------------------------
--------------------------function grab_robot-----------------------------------
--------------------------------------------------------------------------------
function grab_robot()
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value == 1 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if count_time == 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        count_time = 0
        grabbed_msg = compose(caller.data[1],6)
        robot.range_and_bearing.set_data(grabbed_msg)
        int_state = "final_orientation"
    end
end

--------------------------------------------------------------------------------
---------------------Function orientaion set------------------------------------
--------------------------------------------------------------------------------
function final_orientation()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if #robot.range_and_bearing then
        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[1] == caller.data[1] then

                    direction = robot.range_and_bearing[i].data[3]
            end

        end
    end

    if direction == 7 then --go top/north
        if (sign == 1 and angle < 0.1) or
            (sing == -1 and angle > 6.1) then

            ready_to_go_msg = compose(caller.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 8 then --go left/west
        if (sign == 1 and angle > 1.4 and angle < 1.6) or
            (sign == -1 and angle > 4.6 and angle < 4.7) then

            ready_to_go_msg = compose(caller.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 9 then --go down/south
        if angle > 3 and angle < 3.2 then
            ready_to_go_msg = compose(caller.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 10 then --go right/west
        if (sign == 1 and angle > 4.6 and angle < 4.7) or
            (sign == -1 and angle > 1.4 and angle < 1.6) then
            ready_to_go_msg = compose(caller.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    end
end
--------------------------------------------------------------------------------
